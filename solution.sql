-- 1. Create a solution.sql file inside s04 project and do the following using the music_db database:
    -- a.Find all artists that has letter D in its name.
    SELECT * FROM artists WHERE name LIKE "%D%";
    -- b. Find all songs that has a length of less than 230.
    SELECT * FROM songs WHERE length < 230;
    -- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
    SELECT album_title, song_name, length
    FROM albums 
    JOIN songs ON albums.id = songs.id;

    -- d. Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
    SELECT * FROM artists
    JOIN albums ON artists.id = albums.id
    WHERE album_title LIKE "%A%"

    -- e. Sort the album in Z-A order. (SHOW only the first 4 records.)
    SELECT * FROM albums ORDER BY album_title DESC;

    -- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)
    SELECT * FROM albums 
    JOIN songs ON albums.id = songs.id
    ORDER BY album_title DESC, song_name ASC;
    